<?php
/**
 * Description of justice
 *
 * @author Jan
 */
class justice {
    
    static function lustrace($name){
        if(ctype_digit($name)){$ic = $name; $name='';} else {$name = urlencode($name);} // found if its company name or ic
        $url = 'https://isir.justice.cz/isir/ueu/vysledek_lustrace.do?ceuprob=x&nazev_osoby='.$name.'&jmeno_osoby=&ic='.$ic.'&datum_narozeni=&rc=&mesto=&cislo_senatu=&bc_vec=&rocnik=&id_osoby_puvodce=&druh_stav_konkursu=&datum_stav_od=&datum_stav_do=&aktualnost=AKTUALNI_I_UKONCENA&druh_kod_udalost=&datum_akce_od=&datum_akce_do=&nazev_osoby_f=&nazev_osoby_spravce=&rowsAtOnce=50&spis_znacky_datum=';
        $html = file_get_contents($url); // get results
        preg_match("'<!-- hlvaicka stranky -->(.*?)<!-- paticka stranky -->'si", $html, $html); // parse body from results
        
        //parse number of results
        preg_match("'<table width=\"400\" class=\"vysledekLustrace\" cellpadding=\"0\" style=\"margin-left:20px;margin-bottom:20px;\" >(.*?)</table>'si", $html[0], $info);
        $info = explode('<tr>', $info[1]);
        preg_match("'<b>(.*?)</b>'si", $info[4], $info);
        $return['number'] = $info[1];

        //get details links
        preg_match_all('/<a[^>]+href=([\'"])(.+?)\1[^>]*>/i', $html[1], $hrefs);
        foreach($hrefs[2] as $key => $href){
            if($key%2 == 0) {
                $links[] = $href;
            }
        }
        
        //get all finished and unfinished results
        preg_match_all("'<span class=\"skoncenyVysledekLustrace\">(.*?)</span>'si", $html[1], $finished);  
        $i = 0;
        foreach($finished[0] as $key => $d){
            switch($key - ($i*7)){
                case 2:
                    $data[$i]['nazev'] = $d;
                    break;
                case 3:
                    $data[$i]['ic'] = $d;
                    break;
                case 5:
                    $data[$i]['adresa'] = $d;
                    break;
                case 6:
                    $data[$i]['stav'] = $d;
                    break;
                case 7:
                    $i++;
                    break;
            }
        }
        
        $k = $i + 1;
        $i = 0;
        
        preg_match_all("'<span class=\"vysledekLustrace\">(.*?)</span>'si", $html[1], $unfinished);
        foreach($unfinished[0] as $key => $d){
            switch($key - ($i*7)){
                case 2:
                    $data[$i+$k]['nazev'] = $d;
                    break;
                case 3:
                    $data[$i+$k]['ic'] = $d;
                    break;
                case 5:
                    $data[$i+$k]['adresa'] = $d;
                    break;
                case 6:
                    $data[$i+$k]['stav'] = $d;
                    break;
                case 7:
                    $i++;
                    break;
            }
        }
        
        //get links and data together
        foreach ($data as $key => $d){
            $data[$key]['link'] = $links[$key];
        }
        
        $return['data'] = $data;
        
        return $return;
    
    }

}